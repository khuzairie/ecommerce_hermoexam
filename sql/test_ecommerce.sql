/*
Navicat MySQL Data Transfer

Source Server         : localAzwan
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : test_ecommerce

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2017-10-11 10:29:32
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `brand`
-- ----------------------------
DROP TABLE IF EXISTS `brand`;
CREATE TABLE `brand` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_brand` varchar(50) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of brand
-- ----------------------------
INSERT INTO `brand` VALUES ('1', 'ASUS', null);
INSERT INTO `brand` VALUES ('2', 'NIKON', null);
INSERT INTO `brand` VALUES ('3', 'SAMSUNG', null);
INSERT INTO `brand` VALUES ('4', 'Dell', null);

-- ----------------------------
-- Table structure for `migrations`
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES ('1', '2017_10_10_145440_create_users_product', '1');
INSERT INTO `migrations` VALUES ('2', '2017_10_10_190546_create_brand', '2');
INSERT INTO `migrations` VALUES ('3', '2017_10_10_190815_create_order_details', '3');

-- ----------------------------
-- Table structure for `order_details`
-- ----------------------------
DROP TABLE IF EXISTS `order_details`;
CREATE TABLE `order_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `order_number` varchar(50) DEFAULT NULL,
  `unit_price` varchar(50) DEFAULT NULL,
  `quantity` varchar(10) DEFAULT NULL,
  `total_price` varchar(50) DEFAULT NULL,
  `promotion_code` varchar(40) DEFAULT NULL,
  `discaunt` varchar(40) DEFAULT NULL,
  `delivery_to` varchar(40) DEFAULT NULL,
  `shiping_fee` varchar(40) DEFAULT NULL,
  `grand_total` varchar(40) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `product_code` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of order_details
-- ----------------------------
INSERT INTO `order_details` VALUES ('1', 'Mouse Optical', 'ORD-000001', '799', '1', '799', 'GIVEME15', '15', 'Singapore', '0', '784', null, '2017-10-09 19:07:58', '2017-10-09 19:07:58', 'a104');
INSERT INTO `order_details` VALUES ('2', 'Mouse Optical', 'ORD-000002', '799', '1', '799', null, '0', 'Brunei', '0', '799', null, '2017-10-09 19:33:35', '2017-10-09 19:33:35', 'a104');
INSERT INTO `order_details` VALUES ('3', 'Mouse Optical', 'ORD-000003', '799', '2', '1598', null, '0', 'Malaysia', '0', '1598', null, '2017-10-09 19:34:08', '2017-10-09 19:34:08', 'a104');
INSERT INTO `order_details` VALUES ('4', 'Mouse Optical', 'ORD-000004', '799', '2', '1598', null, '0', 'Malaysia', '0', '1598', null, '2017-10-09 19:38:13', '2017-10-09 19:38:13', 'a104');
INSERT INTO `order_details` VALUES ('5', 'Mouse Optical', 'ORD-000005', '799', '1', '799', null, '0', 'Malaysia', '0', '799', null, '2017-10-09 19:38:29', '2017-10-09 19:38:29', 'a104');
INSERT INTO `order_details` VALUES ('6', 'Nikon X510', 'ORD-000006', '20', '1', '20', null, '0', 'Malaysia', '10', '30', null, '2017-10-09 19:39:00', '2017-10-09 19:39:00', 'a102');
INSERT INTO `order_details` VALUES ('7', 'Nikon X510', 'ORD-000007', '20.20', '1', '20.2', null, '0', 'Malaysia', '10', '30.2', null, '2017-10-09 19:40:18', '2017-10-09 19:40:18', 'a102');
INSERT INTO `order_details` VALUES ('8', 'Asus Ultra Notebook', 'ORD-000008', '100', '1', '100', null, '0', 'Malaysia', '10', '110', null, '2017-10-10 21:21:18', '2017-10-10 21:21:18', 'a101');
INSERT INTO `order_details` VALUES ('9', 'Asus Ultra Notebook', 'ORD-000009', '100', '1', '100', null, '0', 'Malaysia', '10', '110', null, '2017-10-10 21:21:28', '2017-10-10 21:21:28', 'a101');
INSERT INTO `order_details` VALUES ('10', 'Asus Ultra Notebook', 'ORD-000010', '100', '2', '200', null, '0', 'Malaysia', '0', '200', null, '2017-10-10 21:21:45', '2017-10-10 21:21:45', 'a101');

-- ----------------------------
-- Table structure for `product`
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `brand` int(11) DEFAULT NULL,
  `selling_price` varchar(50) DEFAULT NULL,
  `retail_price` varchar(50) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `available_quantity` int(11) DEFAULT NULL,
  `image` longtext,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `product_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of product
-- ----------------------------
INSERT INTO `product` VALUES ('1', 'Asus Ultra Notebook', '1', '100', '150', '15', '10', 'images/prv/product-3.jpg', null, 'a101');
INSERT INTO `product` VALUES ('2', 'Nikon X510', '2', '20.20', '25.20', '14', '10', 'images/prv/product-1.jpg', null, 'a102');
INSERT INTO `product` VALUES ('3', 'Samsung Galaxy', '3', '540', '510.20', '14', '10', 'images/prv/product-2.jpg', null, 'a103');
INSERT INTO `product` VALUES ('4', 'Mouse Optical', '4', '799', '700', '14', '10', 'images/prv/product-4.jpg', null, 'a104');
