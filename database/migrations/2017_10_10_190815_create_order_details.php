<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
         Schema::create('order_details', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('order_number');
            $table->string('unit_price');
            $table->string('quantity');
            $table->string('total_price');
            $table->string('promotion_code');
            $table->string('discaunt');
            $table->string('delivery_to');
            $table->string('shiping_fee');
            $table->string('grand_total');
            $table->string('product_code');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('order_details');
    }
}
