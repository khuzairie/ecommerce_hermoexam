<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */


Route::get('/', 'LandingPage\LandingController@index')->name('home');
Route::post('/checkout', 'LandingPage\LandingController@checkout')->name('checkout');
Route::post('/checkout_process', 'LandingPage\LandingController@checkout_process')->name('checkout_process');
  Route::get('/summary_checkout/{id}', 'LandingPage\LandingController@summary_checkout')->name('summary_checkout');