<?php 
namespace App;
use Illuminate\Support\Facades\Auth;

trait Updater { 
   protected static function boot() { parent::boot();
      static::creating(function($model) { 
            if(!empty(Auth::user())){
                $model->created_by = Auth::user()->id;
                $model->updated_by = Auth::user()->id;
            }
        });
 
        static::updating(function($model)  {
            if(!empty(Auth::user())){
                $model->updated_by = Auth::user()->id;
            }
        });

        // static::deleting(function($model)  {
        //     $model->deleted_by = Auth::user()->id;
        //     $model->save();
        // });
    }
}