<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Brand extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    protected $table      = 'brand';
    protected $primaryKey = 'id';
    protected $dates = ['deleted_at'];
}
