<?php

namespace App\Http\Middleware;

use Closure;
use App\CfgMenu;
use Illuminate\Support\Facades\Auth;

class CreateMenu
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!empty(Auth::user()->id))
        {
            $mps_staff_id = Auth::user()->id;
            $menuList=\DB::table(\DB::raw("
              cfg_menu
              left join users on users.id = $mps_staff_id
              left join cfg_menu_sub on cfg_menu_sub.cfg_menu_id_sub = cfg_menu.id
              join cfg_user_access on (
              cfg_user_access.cfg_menu_id = cfg_menu.id AND
              cfg_user_access.user_id = users.id
              )
              inner join
              (
              select id, sorting as parent_group
              from cfg_menu
              where cfg_menu.deleted_at IS NULL
              ) km on km.id = cfg_menu_sub.cfg_menu_id_parent
              ")) 
            ->selectRaw('cfg_menu.id,
              cfg_user_access.id as cfg_user_access_id,
              cfg_menu.icon,
              cfg_menu.menu_name, 
              cfg_menu.url, 
              cfg_menu.menu_level, 
              cfg_menu.sorting,
              cfg_menu.is_placeholder,
              cfg_menu_sub.cfg_menu_id_parent,
              km.parent_group,
              cfg_user_access.deleted_at')
            ->whereNull('cfg_menu.deleted_at')
            ->whereNull('cfg_user_access.deleted_at')
            ->whereNull('cfg_menu_sub.deleted_at')
            ->orderBy('km.parent_group', 'asc')
            ->orderBy('cfg_menu.menu_level', 'asc')
            ->orderBy('cfg_menu.sorting', 'asc')
            ->get();

            \Menu::make('MyNavBar', function($menu) use ($menuList){
                foreach($menuList as $menu_list)
                {
                    if($menu_list->menu_level == 1)
                        $level = "one";
                    elseif($menu_list->menu_level == 2)
                        $level = "second";
                    elseif($menu_list->menu_level == 3)
                        $level = "third";
                    
                    if(!empty($menu_list->url))
                    {
                        if($menu_list->menu_level == 1)
                            $menu->add($menu_list->menu_name, array('route' => $menu_list->url,'level' => $level))->nickname($menu_list->id);
                        else
                            $menu->get($menu_list->cfg_menu_id_parent)->add($menu_list->menu_name, array('route' => $menu_list->url,'level' => $level))->nickname($menu_list->id);
                    }
                    elseif(empty($menu_list->url))
                    {
                        if($menu_list->menu_level == 1)
                            $menu->add($menu_list->menu_name, array('level' => $level))->nickname($menu_list->id);
                        else
                            $menu->get($menu_list->cfg_menu_id_parent)->add($menu_list->menu_name, array('level' => $level))->nickname($menu_list->id);
                    }
                }
            });
        }
        return $next($request);
    }
}
