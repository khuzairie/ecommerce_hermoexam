            <div class="pg-opt">
              <div class="container">
                <div class="row">
                  <div class="col-md-6">
                    <h2>Step 1</h2>
                  </div>
                </div>
              </div>
            </div>
            <section class="slice bg-white">
              <div class="wp-section">
                <div class="container">
                  <div class="row">
                    <div class="col-md-12">

                      <div class="row">
                        <!-- Product list -->
                        
                         
                         @foreach($sql as $k)
                          <form method="post" action="{{URL::route('checkout')}}" method="post">
                          {{ csrf_field() }}
                         <div class="col-md-3">
                          <div class="wp-block product">
                            <figure>
                             <img alt="" src="{{ $k->image }}" class="img-responsive img-center">
                           </figure>
                           <h2 class="product-title"><center><a href="">{{$k->name_brand}} </a></center></h2>
                           <h2 class="product-title"><a href=""><center>{{$k->name}}</center></a></h2>
                           <p>
                            <center>RM{{$k->selling_price}}</center>
                          </p>
                          <p>
                            <center><s>RM{{$k->retail_price}}</s></center>
                          </p>
                          <input type="hidden" name="id_product" value="{{$k->id}}">
                          <input type="hidden" name="selling_price" value="{{$k->selling_price}}">
                          <div class="wp-block-footer">
                            <span class="price pull-left">
                             <select class="form-control " name="quantity">
                              <option value="1">1</option>
                              <option value="2">2</option>
                              <option value="3">3</option>
                              <option value="4">4</option>
                              <option value="5">5</option>
                              <option value="6">6</option>
                              <option value="7">7</option>
                              <option value="8">8</option>
                              <option value="9">9</option>
                              <option value="10">10</option>
                            </select>
                          </span>

                          <button type="submit" class="btn btn-md pull-right">Buy Now</button>


                        </div>
                      </div>
                    </div>
                    </form>
                    @endforeach
                  
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>