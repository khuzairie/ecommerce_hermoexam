<?php

return array (
  'Recruitment Agency Registration' => 'Pendaftaran Agen Rekrutmen',
  'Employment Agent Registration' => 'Pendaftaran Agen Tenaga Kerja',
  'employer individu registration' => 'Pendaftaran individu majikan',
  'employer company registration' => 'Pendaftaran perusahaan majikan',
  'upload maid biodata\\employee (general)' => 'Upload pembantu biodata \\ karyawan (umum)',
  'upload maid biodata (family)' => 'Upload pembantu biodata (keluarga)',
  'upload maid biodata (question)' => 'Upload biodata pembantu (pertanyaan)',
  'upload workers biodata\\employee (general)' => 'Upload pekerja biodata \\ karyawan (umum)',
  'freelance registration' => 'Pendaftaran freelance',
  'Employee registration (Informal)' => 'Pendaftaran karyawan (Informal)',
  'Employee registration (Formal)' => 'Pendaftaran karyawan (Formal)',
);
