<?php

return array (
  'frontend' => 
  array (
    'about_us' => 'Tentang Kami',
    'contact_us' => 'Hubungi Kami',
    'login' => 'Masuk',
    'our_gallery' => 'Galeri Kami',
    'our_roles' => 'Peran Kami',
    'register' => 'Daftar',
  ),
  'general' => 
  array (
    'home' => 'Rumah',
  ),
);
