<?php

return array (
  'frontend' => 
  array (
    'login' => 'Login',
    'register' => 'Register',
    'about_us' => 'About Us',
    'our_roles' => 'Our Roles',
    'our_gallery' => 'Our Gallery',
    'contact_us' => 'Contact Us',
  ),
  'general' => 
  array (
    'home' => 'Home',
  ),
);
