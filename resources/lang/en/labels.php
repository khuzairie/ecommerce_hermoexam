<?php

return array (
  'frontend' => 
  array (
    'footer' => 
    array (
      'quicklinks' => 'Quicklinks',
      'home' => 'Home',
      'about_us' => 'About Us',
      'contact_us' => 'Contact Us',
      'term' => 'Term',
      'privacy' => 'Privacy policy',
      'copyright' => 'Copyright Notice',
      'follow_us' => 'Follow Us',
      'all_right' => 'All rights reserved.',
      'term_service' => 'Term Service',
    ),
    'auth' => 
    array (
      'email' => 'Email',
      'password' => 'Password',
      'forgot_password' => 'Forgot Password',
      'enter' => 'Enter',
      'close' => 'Close',
    ),
    'frontpage' => 
    array (
      'book_your_maid' => 'BOOK YOUR MAID',
      'now' => 'NOW',
      'looking_for' => 'Here\'s what you\'re looking for',
      'register_now' => 'REGISTER NOW',
      'online_real' => 'ONLINE AND REAL-TIME',
      'user_will_notify' => 'Users will be notified for every transaction made through the system.',
      'double_bio' => 'NO SIMILAR BIODATA GUARANTEE',
      'high_quality' => 'High Quality',
      'complete_facility' => 'With complete facilities ready at our training center, all candidates are well trained and exposed to the latest equipment and technology.',
      'why_book_with_us' => 'WHY BOOKING WITH US? - WHAT MAKES US BETTER?',
      'book_and_pay' => 'Book and Pay Online with Ease',
      'save_time' => 'Save Time and Effort',
      'business_transacted' => 'Business transacted locally',
      'money_safe' => 'Your Money is Secure',
      'satisfaction' => 'Satisfaction is assured',
      'collaboration' => 'Collaboration',
      'worker_gallery' => 'Gallery Workers',
      'maid' => 'Maid',
      'name' => 'Name',
      'id' => 'ID',
      'age' => 'Age',
      'country' => 'Country',
      'information' => 'Information',
      'close' => 'Close',
    ),
    'register' => 
    array (
      'registration' => 'Registration',
      'please_choose' => 'Please Choose',
      'recruitment' => 'Recruitment',
      'agent' => 'Agent',
      'freelance' => 'Freelance',
      'employer_individu' => 'Employer Individu',
      'employer_company' => 'Employer Company',
    ),
    'form' => 
    array (
      'company name' => 'company name',
      'country' => 'Country',
      'mobile phone' => 'Mobile Phone',
      'office telephone number' => 'Office Telephone Number',
      'email' => 'Email',
      'contact person' => 'Contact Person',
      'please_select' => 'Please select',
      'please_insert' => 'Please insert',
      'contact no person' => 'Contact no person',
      'name' => 'Name',
      'address' => 'Address',
      'freelance type' => 'Freelance Type',
      'id number' => 'ID Number',
      'type of user' => 'Type of User',
      'postcode' => 'Postcode',
      'state' => 'State',
      'company registration no' => 'company registration no',
      'company type' => 'Company type',
      'business type' => 'Business type',
      'office fax no' => 'Office fax no',
      'nama PT' => 'Name of PT',
      'provinsi - daerah' => 'Provinces',
      'nomor telp kantor' => 'Office phone number',
      'alamat kantor' => 'Alamat kantor',
      'negara' => 'Country',
      'alamat email' => 'Email address',
      'email address' => 'Email Address',
      'contact person number' => 'Contact person number',
      'contact number' => 'Contact Number',
      'religion' => 'Religion',
      'business sector' => 'Business Sector',
    ),
    'terms_condition' => '<p><strong>By checking the box, you hereby agree and undertake to comply with the provisions of the Personal Data Protection Act 2010 insofar as you process the personal data of the foreign Worker(s) employed by you. You further warrant that you have obtained all necessary consents from your employees/foreign Worker(s) for their personal data to be processed by the </strong><em><strong>eforeignworkers</strong></em> <strong> operated and managed by Jaguh Sensasi Sdn Bhd and have read and understood the aforesaid&nbsp;</strong><a><u><strong>Terms &amp; Conditions</strong></u></a><strong>&nbsp;for using this facility.</strong></p>
                <p>&nbsp;</p>
                <p align="center"><u><strong>Acceptable Use Policy/Disclaimer</strong></u></p>
                <p>Thank you for visiting <em><strong>eforeignworkers</strong></em> website. The website is owned and operated by Jaguh Sensasi Sdn Bhd.Your use of this Website (<em><strong>eforeignworkers</strong></em>.com.my) and/or Services for Foreign Worker Management is subject to you agreeing to be bound by and to comply with this Acceptable Use Policy ("Policy"). If you do not agree with the Policy you should exit from this Website and/or Other Services or Applications instantly.</p>
                <p>&nbsp;</p>
                <p align="center"><u><strong>Copyright / Trademarks</strong></u></p>
                <p>This portal <em><strong>eforeignworkers</strong></em>.com.my and Other Services or Applications for Foreign Worker Management and the contents, including text, graphics, button icons, digital downloads, data compilations, logos and information regarding the status of <em><strong>eforeignworkers</strong></em>, etc. are proprietary to Jaguh Sensasi Sdn Bhd or its content suppliers. You may use this portal and the contents only for its intended professional purpose and non-commercial purposes subject to applicable copyright laws.</p>
                <p>All trademarks, trade names, service marks (whether registered or unregistered) and logos in this portal are owned by Jaguh Sensasi Sdn Bhd or their respective owners and must not be used or modified in any way without obtaining the prior written consent of Jaguh Sensasi Sdn Bhd.</p>
                <p>This portal and Other Services or Applications for Foreign Worker Management and the products, technology and processes contained may be the subject of other intellectual property rights owned by Jaguh Sensasi Sdn Bhd or by third parties. No licence is granted in respect of such intellectual property rights other than as set out in this Policy. Your use of this portal and/or Other Services or Applications for Foreign Worker Management and the products, technology and processes contained may be the subject of other intellectual property rights owned must not in any way infringe the intellectual property rights of any person.</p>
                <p align="center">&nbsp;</p>
                <p align="center"><u><strong>Content</strong></u></p>
                <p>By using this portal <em><strong>eforeignworkers</strong></em>.com.my, and/or Other Services or Applications for Foreign Worker Management or any particular Service including the posting of content or other public communication forums hosted on this Website, you agree that you will not upload, post or otherwise transmit or make available any content (including text links, communications, software, images, sounds, data or other information) that is not consistent with the permissible uses outlined in this Policy or the relevant Specific Terms of Use, including, but not limited to the following:</p>
                <ul>
                <li>False, inaccurate, misleading, unlawful, harmful, threatening, abusive, tortuous, harassing, defamatory, libelous, invasive of another\'s privacy, vulgar, profane, sexually oriented/suggestive/explicit, obscene, racially or ethnically offensive or otherwise objectionable.</li>
                <li>Violates any laws, third party rights, or infringes on any patent, trademark, trade secret, copyright or other proprietary rights of any party.</li>
                <li>Constitutes advertising or promotion, junk mail, spamming, chain letters or any other form of unauthorized solicitation. Links that connect to commercial websites will not be considered unauthorized solicitations unless the link or the website content appears to be intended as a means of solicitation as determined by Jaguh Sensasi Sdn Bhd in its sole discretion.</li>
                <li>Contains software viruses, Trojan horses, worms, time bombs, cancelbots or any other computer code or files that are designed to disrupt damage or limit the functioning of any software or hardware.</li>
                <li>Provides any non-public information about <em><strong>eforeignworkers</strong></em> or any other company or person without the proper authorization to do so. This includes providing the names, addresses and extension numbers of Jaguh Sensasi employees and any of the users or participants of the Services.</li>
                <li>Impersonates any person or entity, including Jaguh Sensasi Sdn Bhd employees or forges any TCP-IP packet header, email header or any part of a message header. This prohibition does not include the use of aliases or anonymous remailers.</li>
                <li>Suggests or encourages illegal activity.</li>
                </ul>
                <p>&nbsp;</p>
                <p align="center"><u><strong>Email and Other Forms of Electronic Communication</strong></u></p>
                <p>When you have requested/submitted/initiated any application on <em><strong>eforeignworkers</strong></em> related documents/messages would be sent to you by email*, or communication through mobile phone* short messages service (SMS), failure to receive it does not constitute an unsuccessful processing of your application. An application is deemed successful and complete after a final confirmation has been received on your authorized email address. Jaguh Sensasi Sdn Bhd is not responsible for the delivery of any electronic communication.</p>
                <p>* Email ID of "Employer or Representative or any similar authorized personnel" needs to be registered with along with the official hand phone number for at least 1 User and 1 PIC (up to 3 PICs is acceptable).</p>
                <p><strong>Hand Phone number of User</strong>&nbsp;is required as SMS information would be sent from the <em><strong>eforeignworkers</strong></em> to the User/PIC for processing of applications that would be sent from the system for each of the application submissions through <em><strong>eforeignworkers</strong></em>.</p>
                <p>Any changes to email, handphone, mobile numbers employer˙ needs to submit this changes request. Successful confirmation of the change, please provide 24 hours for <em><strong>eforeignworkers</strong></em> to reflect the changes in the system.</p>
                <p>&nbsp;</p>
                <p align="center"><u><strong>Links</strong></u></p>
                <p>This Website <em><strong>eforeignworkers</strong></em>.com.my and Other Services or Applications for Foreign Worker Management may contain links to websites maintained by third parties ("External Sites"). These External Sites are provided for user\'s reference and convenience only. Jaguh Sensasi Sdn Bhd does not operate, control or endorse in any respect such External Sites or their content. User assumes sole responsibility for use of these External Sites and are therefore, advised to examine the terms and conditions of those External Sites carefully.</p>
                <p>You must not link to this portal <em><strong>eforeignworkers</strong></em>.com.my and/or Other Services or Applications for Foreign Worker Management without obtaining the prior written approval of Jaguh Sensasi Sdn Bhd. Use of any automated screen capture or screen scraping technologies to obtain information from this site without the prior written approval of Jaguh Sensasi Sdn Bhd is an infringement of Jaguh Sensasi Sdn Bhd\'s intellectual property rights.</p>
                <p>&nbsp;</p>
                <p align="center"><u><strong>Security</strong></u></p>
                <p>When you use or access your Login information, a secure server is used. Secure Server Layer (SSL) encrypts the information you send through this portal and/or Other Services or Applications for any of the applications/payments. Although Jaguh Sensasi Sdn Bhd would use its best endeavor to ensure security, Jaguh Sensasi Sdn Bhd makes no warranty in respect of the strength or effectiveness of that encryption and Jaguh Sensasi Sdn Bhd is not and shall not be held responsible for events arising from unauthorized access of the information you provide.</p>
                <p>&nbsp;</p>
                <p align="center"><u><strong>Disclaimer</strong></u></p>
                <p>The content on this Website <em><strong>eforeignworkers</strong></em>.com.my and Other Services or Applications for Foreign Workers Management are provided on an "as is" and "as available" basis. You therefore agree that you use this Website and/or Other Services or Applications for Foreign Workers Management and the services provided at your sole risk. Jaguh Sensasi Sdn Bhd, its officers, directors, employees, partners and suppliers disclaim:</p>
                <ul>
                <li>All liability to you or anyone else for any direct, indirect, special, consequential or punitive loss or damage (including without limitation loss of revenue, loss of goodwill, loss of reputation and loss of, or damage to, data), incurred or suffered in connection with the use or inability to use this Website and/or Other Services or Applications for Foreign Workers Management whether based in contract, tort or otherwise even if we are informed of their possibility;</li>
                <li>All liability to you or anyone else if interference with or damage to your computer systems occurs in connection with use of this Website and/or Other Services or Applications for Foreign Workers Management or an external site. You must take your own precautions to ensure that whatever you select for use from this Website and/or Other Services or Applications is free of viruses or anything else that may interfere with or damage the operation of your computer systems.</li>
                </ul>
                <p><br /> </p>
                <p align="center"><u><strong>Privacy Policy</strong></u></p>
                <p>We regard your privacy as important. The data with <em><strong>eforeignworkers</strong></em> is taken from with any changes to the data needs to be requested to Jaguh Sensasi Sdn Bhd, which would then be sent to <em><strong>eforeignworkers</strong></em> by Jaguh Sensasi Sdn Bhd Administrator.</p>
                <p>Jaguh Sensasi Sdn Bhd collects the following information from :</p>
                <ul>
                <li>Employer and/or other company details including employer name, title, business address and contact information</li>
                <li>PIC ( Person In charge) details includes Name, business addresses and telephone numbers</li>
                <li>Email addresses, fax numbers and mobile numbers associated</li>
                <li>Information collected about you and your organization may be shared with:</li>
                <li>Payment Gateways / e-commerce</li>
                <li><strong>Authorised Bank</strong>&nbsp;for transaction and payment authorization</li>
                <li><strong>Others</strong></li>
                </ul>
                <p>&nbsp;</p>
                <p align="center"><u><strong>General Practices Regarding Use &amp; Application Processes</strong></u></p>
                <p>You acknowledge that Jaguh Sensasi Sdn Bhd may establish general practices and limits concerning use of the <em><strong>eforeignworkers</strong></em> Application, including without limitation the maximum number of days any uploaded content / information will be retained on the system, the maximum disk space that will be allotted on Jaguh Sensasi Sdn Bhd\'s servers on your behalf, and the maximum number of times (and the maximum duration for which) you may access the <em><strong>eforeignworkers</strong></em> Application process in a given period of time. This would be notified to you through email on your registered email address (as mentioned above under&nbsp;<u>Email and Other Forms of Electronic Communication</u>).</p>
                <p>&nbsp;</p>
                <p align="center"><u><strong>Personal Data Protection Act 2010</strong></u></p>
                <p>You hereby agree and undertake to comply with the provisions of the Personal Data Protection Act 2010 insofar as you process the personal data of the foreign workers employed by you. You further warrant that you have obtained all necessary consents from your employees/foreign workers for their personal data to be processed by the <em><strong>eforeignworkers</strong></em> operated and managed by Jaguh Sensasi Sdn Bhd.</p>
                <p>&nbsp;</p>
                <p>I hereby agree that Jaguh Sensasi Sdn Bhd has no responsibility or liability for the deletion or failure to store any messages and other communications or other content maintained or transmitted by <em><strong>eforeignworkers</strong></em>. I agree that Jaguh Sensasi Sdn Bhd shall not be liable for any damage or loss, direct or consequential, arising out of any lack of access to the internet site or delay or non-receipt of E-mail alerts at any time by me.</p>
                <p>&nbsp;</p>
                <p><strong>Close</strong></p>',
  ),
);
