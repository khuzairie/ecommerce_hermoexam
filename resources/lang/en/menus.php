<?php

return array (
  'language-picker' => 
  array (
    'language' => 'Languages',
    'langs' => 
    array (
      'en' => 'English',
      'id' => 'Indonesian',
    ),
  ),
);
