<input type="hidden" name="content_id" value="<?php echo e($entity_content->content_id); ?>">
<input type="hidden" name="entity_id" value="<?php echo e($entity_content->entity_id); ?>">
<?php $__currentLoopData = $getform; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $q1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<div class="col-md-6">  
    <div class="form-group">
        <label class="control-label col-md-4 text-uppercase"><?php echo e($q1->field_name); ?></label>
        <div class="col-sm-9 col-md-7">
           <?php if($q1->field_input_type == 'text'): ?>                 
            <input type="text" class="form-control" name="<?php echo e(str_replace(' ', '_', $q1->field_name)); ?>"
            <?php if(!empty($entity_content->meta[$q1->form_details_id])): ?>
            value="<?php echo e($entity_content->meta[$q1->form_details_id]); ?>"
            <?php endif; ?> 
            placeholder="Please insert <?php echo e($q1->field_name); ?>">

            <?php elseif($q1->field_input_type == 'dropdown'): ?>
            <?php
                $child = App\Models\CfgAttributeDetails::selectRaw("id,name")
                ->where('cfg_attribute_id',$q1->cfg_attribute_id)
                ->whereNull('deleted_at')
                ->get();
            ?>
            <select class="form-control text-uppercase" name="<?php echo e(str_replace(' ', '_', $q1->field_name)); ?>" id="<?php echo e(str_replace(' ', '_', $q1->field_name)); ?>_selectid">
                <?php $__currentLoopData = $child; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cd): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <option value="<?php echo e($cd->id); ?>"
                   <?php if(!empty($entity_content->meta[$q1->form_details_id])): ?>
                   <?php if($cd->id == $entity_content->meta[$q1->form_details_id]): ?> selected <?php endif; ?>
                   <?php endif; ?>
                   ><?php echo e(strtoupper($cd->name)); ?></option>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </select>

            <?php elseif($q1->field_input_type == 'textarea'): ?>
            <textarea class="form-control" rows="3" cols="5" name="<?php echo e(str_replace(' ', '_', $q1->field_name)); ?>"><?php if(!empty($entity_content->meta[$q1->form_details_id])): ?><?php echo e($entity_content->meta[$q1->form_details_id]); ?><?php endif; ?></textarea>
           <?php endif; ?>
            <span class="label label-block label-danger" id="<?php echo e(str_replace(' ', '_', $q1->field_name)); ?>"></span>                 
        </div>
    </div>
</div>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>